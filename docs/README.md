# Introduction #
My name is Amitava Mudi. I live in Kolkata with my wife and two kids. My son is 10 years old, and daughter is 5. My wife is a food blogger, fashion designer, writer(she writes on food for Anandabazar Patrika, and other online and offline new papers.

### Experience ###
I have more than 20 years of experience as on October, 2019.

- Junior Suprrvisor Service(Electronics) 1.5 years
- Software Analyst(Embedded systems development using Linux and C) - 9 months
- Sr Software engineer(Embedded systems development using C, UI development using Java, and C#.NET(2.5 years)
- Software engineer(UI application development for Set top boxes using Java. (1 year)
- Technical Leader (and then Architect) worked on C, Java, HTML and Java script. Embedded systems software development. (5.5 years)
- Senior Architect: Worked on Java, Javascript, C, Linux, Android and Python etc. (7 Years)
- Release train engineer: Worked on managing multiple teams. (6 months)
- Senior Architect: Android, HTML, JS(Bootstrap), Ruby/Ruby on Rails, and AWS etc. (1 year)

### Education ###

- Diploma in Electronics and telecommunications
- Advanced certificate in Software Engineering.
- Bachelor or Arts
- Master of Science(Computer Science)
- Master of Business Administrarion(MBA). 

### Other details ###

- Provided training on SDK in Turkey, Malaysis, China and India.
- Filed a patent with two other co-inventors.

# Where to start #
To be updated.